include config.mk

all: options sdhcp

options:
	@echo sdhcp build options:
	@echo "CFLAGS   = $(CFLAGS)"
	@echo "LDFLAGS  = $(LDFLAGS)"
	@echo "CC       = $(CC)"

$(OBJ): config.mk

sdhcp: sdhcp.c util.c
	$(CC) $^ -o $@ $(CFLAGS)

clean:
	rm -f sdhcp $(OBJ)

install: all
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f sdhcp $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	sed "s/VERSION/$(VERSION)/g" < sdhcp.1 > $(DESTDIR)$(MANPREFIX)/man1/sdhcp.1
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/sdhcp.1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/sdhcp\
		$(DESTDIR)$(PREFIX)/bin

.PHONY: all options clean install uninstall
