# sdhcp version
VERSION   = 0.3

# paths
PREFIX    = /usr/local
MANPREFIX = $(PREFIX)/share/man

# flags
CPPFLAGS = -D_DEFAULT_SOURCE
CFLAGS   = -Wall -Wextra -pedantic -std=c99 $(CPPFLAGS)
LDFLAGS  = -s

# compiler and linker
CC = cc
